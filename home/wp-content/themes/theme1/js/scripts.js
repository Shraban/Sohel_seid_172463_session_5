$(function($) {
  var allAccordions = $('.accordion div.data');
  var allAccordionItems = $('.accordion .accordion-item');
  $('.accordion > .accordion-item').click(function() {
    if($(this).hasClass('open'))
    {
      $(this).removeClass('open');
      $(this).next().slideUp("slow");
    }
    else
    {
    allAccordions.slideUp("slow");
    allAccordionItems.removeClass('open');
    $(this).addClass('open');
    $(this).next().slideDown("slow");
    return false;
    }
  });
});



$(document).ready(function() {
    // Using default configuration
    $('#carousel').carouFredSel();

    // Using custom configuration
    $('#carousel').carouFredSel({
        items               : 4,
        direction           : "left",
	height :130,
	responsive: true,
        scroll : {
            items           : 1,
     
            duration        : 500,                         
            pauseOnHover    : true
        }                   
    });
});

